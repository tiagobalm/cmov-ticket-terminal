package pt.up.fe.tiago.ticketterminal.dependencyinjection;

import javax.inject.Singleton;

import dagger.Component;
import pt.up.fe.tiago.ticketterminal.MainActivity;

@Singleton
@Component(modules = {NetworksModule.class})
public interface ApplicationComponent {
    void inject(MainActivity mainActivity);
}
