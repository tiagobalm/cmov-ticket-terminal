package pt.up.fe.tiago.ticketterminal.initializer;

import android.app.Application;

import pt.up.fe.tiago.ticketterminal.dependencyinjection.ApplicationComponent;
import pt.up.fe.tiago.ticketterminal.dependencyinjection.DaggerApplicationComponent;
import pt.up.fe.tiago.ticketterminal.dependencyinjection.NetworksModule;
import pt.up.fe.tiago.ticketterminal.utils.Constants;

public class TicketTerminalApplication extends Application {
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                                .networksModule(new NetworksModule(Constants.URL))
                                .build();
    }

    public ApplicationComponent getApplicationComponent(){
        return applicationComponent;
    }
}
