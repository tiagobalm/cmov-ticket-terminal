package pt.up.fe.tiago.ticketterminal.webservice;

import pt.up.fe.tiago.ticketterminal.webservice.dto.ValidateTicketsDTO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Webservice {

    @POST("tickets/validate")
    Call<String> validateTickets(@Body ValidateTicketsDTO validateTicketsDTO);

}