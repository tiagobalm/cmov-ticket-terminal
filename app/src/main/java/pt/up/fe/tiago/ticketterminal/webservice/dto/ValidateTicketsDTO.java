package pt.up.fe.tiago.ticketterminal.webservice.dto;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class ValidateTicketsDTO implements Serializable {
    private UUID customerUUID;
    private List<UUID> tickets;

    public ValidateTicketsDTO() {}

    public ValidateTicketsDTO(UUID customerUUID, List<UUID> tickets) {
        this.customerUUID = customerUUID;
        this.tickets = tickets;
    }

    public UUID getCustomerUUID() {
        return customerUUID;
    }

    public void setCustomerUUID(UUID customerUUID) {
        this.customerUUID = customerUUID;
    }

    public List<UUID> getTickets() {
        return tickets;
    }

    public void setTickets(List<UUID> tickets) {
        this.tickets = tickets;
    }
}
