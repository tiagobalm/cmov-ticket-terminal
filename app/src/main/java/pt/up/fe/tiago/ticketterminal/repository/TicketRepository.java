package pt.up.fe.tiago.ticketterminal.repository;

import javax.inject.Inject;
import javax.inject.Singleton;

import pt.up.fe.tiago.ticketterminal.webservice.Webservice;
import pt.up.fe.tiago.ticketterminal.webservice.dto.ValidateTicketsDTO;
import retrofit2.Call;

@Singleton
public class TicketRepository {

    private final Webservice webservice;

    @Inject
    TicketRepository(Webservice webservice) { this.webservice = webservice; }

    public Call<String> validateTickets(ValidateTicketsDTO validateTicketsDTO) { return webservice.validateTickets(validateTicketsDTO); }
}
