package pt.up.fe.tiago.ticketterminal;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import pt.up.fe.tiago.ticketterminal.initializer.TicketTerminalApplication;
import pt.up.fe.tiago.ticketterminal.repository.TicketRepository;
import pt.up.fe.tiago.ticketterminal.webservice.dto.ValidateTicketsDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.UUID;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {
    private static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    private TextView responseTextView;

    @Inject
    Gson gson;

    @Inject
    TicketRepository ticketRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((TicketTerminalApplication) this.getApplication()).getApplicationComponent().inject(this);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        FloatingActionButton QRCodeButton = findViewById(R.id.qr_code);
        responseTextView = findViewById(R.id.response);
        QRCodeButton.setOnClickListener(v -> scan());
    }

    public void scan() {

        try {
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE","QR_CODE_MODE" );
            startActivityForResult(intent, 0);
        }
        catch (ActivityNotFoundException e) {
            showDialog(this).show();
        }
    }

    private static AlertDialog showDialog(final MainActivity act) {

        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle("No Scanner Found");
        downloadDialog.setMessage("Download a scanner code activity?");
        downloadDialog.setPositiveButton("Yes", (dialogInterface, i) -> {
            Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            act.startActivity(intent);
        });
        downloadDialog.setNegativeButton("No", null);
        return downloadDialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");

                try {

                    JSONObject qrCodeInformation = new JSONObject(contents);
                    UUID customerUUID = UUID.fromString(qrCodeInformation.get("customeruuid").toString());
                    JSONArray tickets = qrCodeInformation.getJSONArray("tickets");

                    Type UUIDListType = new TypeToken<ArrayList<UUID>>(){}.getType();
                    ArrayList<UUID> events = gson.fromJson(tickets.toString(), UUIDListType);

                    ValidateTicketsDTO validateTicketsDTO = new ValidateTicketsDTO(customerUUID, events);

                    ticketRepository.validateTickets(validateTicketsDTO).enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            responseTextView.setText(response.body());
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) { t.printStackTrace(); }
                    });


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
